# Voraussetzungen
Projekt wurde lokal ausgecheckt und notwendige Tools sind installiert

# Aufgabe 1 - Konfiguration
a) Aktiviere die beiden Actuators ENV und LOGGER in einer externen Konfiguration, starte die Applikation mit der ausgelagerten Konfiguration und teste ob die Endpunkte verfügbar sind

b) Füge dem INFO Actuator das heutige Datum hinzu, starte die Applikation mit dem neuen Profil und teste ob der INFO Endpunkte die Information zur Verfügung stellt

c) Aktiviere für das Package *de.schulung* das LogLevel *debug* und prüfe mit dem Logger Actuator ob die Umstellung funktioniert hat

d) Aktiviere File Logging in logs/schulung_app_log.log und überprüfe ob die Datei angelegt wurde

# Aufgabe 2 - Profiling
Erstelle das Profil local und ändere den Port auf 4711, starte die Applikation mit dem neuen Profil und überprüfe ob die Applikation auf dem neuen Port läuft